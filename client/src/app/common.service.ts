import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { User } from "./user.model";

@Injectable({
  providedIn: "root"
})
export class CommonService {
  constructor(private _http: HttpClient) {
  }

  readonly baseURL = "http://localhost:8000/api/employees";

  list: User[] = [];

  createUser(user: { userName?: string; email?: string; _id: any }) {
    return this._http.post(this.baseURL, user);
  }

  getLatestUser() {
    return this._http.get(this.baseURL).subscribe(res => {
      this.list = res as User[];
    });
  }

  deleteUser(_id: string) {
    // debugger
    //   return this._http.delete(`${this.baseURL}`);
    return this._http.delete(this.baseURL +`/${ _id}`);
    //     return this._http.delete(this.baseURL);
  }


  updateUser(user: { userName?: string; email?: string; _id: string }) {
    return this._http.put(this.baseURL +`/${ user._id}`, user);
  }
}





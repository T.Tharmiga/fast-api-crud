from fastapi import APIRouter

from models.user import Employee
from config.db import mongoUrl

from schemas.user import serializeDict, serializeList

from bson.objectid import ObjectId

router = APIRouter()

database = mongoUrl.employee_db
collections = database.employees


@router.get('/api/employees')
async def find_all_users():
    # print(collections.find())
    # print(serializeList(collections.find()))
    return serializeList(collections.find())


@router.get('/api/employees/{id}')
async def find_one_user(_id):
    return serializeDict(collections.find_one({"_id": ObjectId(_id)}))


@router.post('/api/employees')
async def create_user(employees: Employee):
    collections.insert_one(dict(employees))
    return serializeList(collections.find())


@router.put('/api/employees/{_id}')
async def update_user(_id, employees: Employee):
    collections.find_one_and_update({"_id": ObjectId(_id)}, {"$set": dict(employees)})
    return serializeDict(collections.find_one({"_id": ObjectId(_id)}))


@router.delete('/api/employees/{_id}')
async def delete_user(_id):
    return serializeDict(collections.find_one_and_delete({"_id": ObjectId(_id)}))

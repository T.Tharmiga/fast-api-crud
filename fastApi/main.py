from fastapi import FastAPI, APIRouter

from fastapi.middleware.cors import CORSMiddleware

from routes.user import router
app = FastAPI()

origins = ['http://localhost:4200']

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    # allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=[""],
)


app.include_router(router)

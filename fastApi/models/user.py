from pydantic import BaseModel


class Employee(BaseModel):
    userName: str
    email: str
